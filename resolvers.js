const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";

const { query } = require('nact');

const resource = "task";

module.exports = {

    Mutation:{
        changeTask: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

			if (args.input.executor_id) {
				args.input.executor_id = new ObjectId(args.input.executor_id);
            }
            if (args.input.customer_id) {
				args.input.customer_id = new ObjectId(args.input.customer_id);
            }

            if(args._id){
                return await query(collectionItemActor,  {"type": "task", search:{_id: args._id}, input: args.input }, global.actor_timeout);
            }else {
                return await query(collectionItemActor, {"type": "task", input: args.input}, global.actor_timeout);
            }

        },
    },

    Query: {

        getTask: async (obj, args, ctx, info) => {


            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "task", search: {_id: args.id}}, global.actor_timeout);


        },

        getTasks: async (obj, args, ctx, info) => {

            const collectionActor = ctx.children.get("collection")

            return await query(collectionActor, {"type": "task"}, global.actor_timeout);

        },
    },
	Task : {
		executor:  async (obj, args, ctx, info) => {
			if (obj.executor_id) {
				return (await ctx.db.user.find({_id: new ObjectId(obj.executor_id)}))[0];
			}
        },
        customer:  async (obj, args, ctx, info) => {
			if (obj.customer_id) {
				return (await ctx.db.user.find({_id: new ObjectId(obj.customer_id)}))[0];
			}
		},
	},
}